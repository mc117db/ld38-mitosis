﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

sealed class CloneMassNumberUpdater : MonoBehaviour {

    // Use this for initialization
    public Character clone;
    public TextMesh textMesh;
	void Start () {
        if (!clone)
        {
            clone = transform.parent.GetComponent<Character>();
        }
        clone.MassChangeEvent += UpdateText;
    }
	void UpdateText(int num)
    {
        textMesh.text = num.ToString();
    }
	// Update is called once per frame
	void Update () {
		
	}
}
