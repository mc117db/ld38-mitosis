﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : Character, IDragHandler, IBeginDragHandler,IEndDragHandler,IRecyle {

    //Character Class Members
    //public int Mass = 1;
    //public Vector3 targetVelocity;
    //public float speedMult = 1f;
    //bool isMoving;
    Vector3 initDragPosWorld;
    public static Transform playerTransform;
    int dragPower;
    bool initDrag;
    Vector2 rawDragVector;
    float rawDragMag;
    public void Awake()
    {
        playerTransform = transform;
    } 
    public void Start()
    {
        Restart();
    } 
    public int DragPower
    {
        get
        {
            return dragPower;
        }

        set
        {
            if (dragPower != value)
            {
                dragPower = value;
                if (DragMagnitudeChange != null)
                {
                    DragMagnitudeChange(dragPower);
                }
            }
        }
    }

    public static event OnIntValueChange DragMagnitudeChange;
    public static event OnEvent StartDragEvent;
    public static event OnEvent StartMoveEvent;
    public static event OnEvent EndMoveEvent;
    public static event OnEvent DeathEvent;

    void MoveComplete()
    {
        //Debug.Log("MOVE COMPLETE");
        isMoving = false;
        if (EndMoveEvent != null)
        {
            EndMoveEvent();
        }
        Mass++; // Regen
    }

    // Drag Handler Interfaces
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (isMoving)
        {
            return;
        }
        initDrag = true;
        targetVelocity = Vector2.zero;
        rawDragVector = Vector3.zero;
        rawDragMag = 0;
        DragPower = 0;
        Vector3 inputWorldPos = Mathfx.ToVector3(eventData.position) + new Vector3(0, 0, Vector3.Distance(Camera.main.transform.position, transform.position));
        initDragPosWorld = Camera.main.ScreenToWorldPoint(inputWorldPos);
        //Debug.Log("START DRAG");
        if (StartDragEvent != null)
        {
            StartDragEvent();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isMoving)
        {
            return;
        }
        Vector3 curInput  = Mathfx.ToVector3(eventData.position) + new Vector3(0, 0, Vector3.Distance(Camera.main.transform.position, transform.position));
        rawDragVector = Camera.main.ScreenToWorldPoint(curInput) - initDragPosWorld;
        targetVelocity = Mathfx.ToVector2(rawDragVector.normalized);
        rawDragMag = rawDragVector.magnitude;
        DragPower = Mathf.Clamp(Mathf.RoundToInt(rawDragMag),0,Mathf.RoundToInt((float)Mass/2));
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("END DRAG");
        if (DragPower > 0)
        {
            if (!isMoving && initDrag)
            {
                initDrag = false;
                StartCoroutine(Move(targetVelocity, DragPower, MoveComplete));
                if (StartMoveEvent != null)
                {
                    StartMoveEvent();
                }
            }
        }
    }

    public void Restart()
    {
        Mass = 10;
    }
    public void Shutdown()
    {
        Debug.Log("YOU LOSE!");
        if (DeathEvent != null)
        {
            DeathEvent();
        }
    }
}
