﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody2D))]
public class Character:MonoBehaviour
{
    Rigidbody2D mRigidbody;
    public static int maxMass = 30;
    public static float startScale = 0.5f;
    public static float endScale = 3f;
    int mass = 0;
    public Vector2 targetVelocity;
    public float timeToTravel;
    protected bool isMoving;
    public delegate void OnEvent();
    public delegate void OnIntValueChange(int val);
    public event OnIntValueChange MassChangeEvent;
    public event OnEvent MoveCompleteEvent;
    public int Mass
    {
        get
        {
            return mass;
        }

        set
        {
            if (value != mass && gameObject.activeSelf == true)
            {
                mass = Mathf.Clamp(value, 0, maxMass);
                if (MassChangeEvent != null)
                {
                  MassChangeEvent(mass);
                }
                ScaleAccordingToMass(mass);
            }
            if (mass <= 0)
            {
                GameObjectUtil.Destroy(gameObject);
            }

        }
    }
    public GameObject clonePrefab;
    public GameObject lastSpawn;
    public delegate void OnEventComplete();
    public IEnumerator Move(Vector2 targetVel,int power,OnEventComplete MoveCompleteCallBack)
    {
        if (!mRigidbody)
        {
            mRigidbody = gameObject.GetComponent<Rigidbody2D>();
        }
        if (MoveCompleteCallBack != null)
        {
            SeperateSelf(power,-targetVel);
        }
        isMoving = true;
        Vector3 initPos = transform.position;
        Vector3 targetPos = Mathfx.ToVector2(transform.position) + targetVel*power;
        float elapsedTime = 0;
        while (elapsedTime < timeToTravel)
        {
            mRigidbody.MovePosition(Mathfx.ToVector3(Mathfx.Sinerp(initPos, targetPos, (elapsedTime / timeToTravel))));
            elapsedTime += Time.deltaTime;
            if (elapsedTime < timeToTravel)
            {
                yield return null;
            }
            else
            {
                if (MoveCompleteEvent != null)
                {
                    MoveCompleteEvent();
                }
                if (MoveCompleteCallBack != null)
                {
                MoveCompleteCallBack();
                }
                yield return null;
            }
        }  
    }
    public IEnumerator ScaleTo(Vector3 endScale,float time)
    {
        float elaspedTime = 0;
        Vector3 initScale = transform.localScale;
        while (transform.localScale != endScale)
        {
            transform.localScale = Mathfx.Berp(initScale, endScale, elaspedTime / time);
            elaspedTime += Time.deltaTime;
            yield return null;
        }
    }
    void ScaleAccordingToMass(int mass)
    {
        StopCoroutine("ScaleTo");
        float lerpFactor = Mathf.Clamp01((float)mass / (float)maxMass);
        //Debug.Log(lerpFactor);
        StartCoroutine(ScaleTo(Vector3.Lerp(Vector3.one * startScale, Vector3.one * endScale, lerpFactor),0.2f));
    }
    void SeperateSelf(int subtractAmount,Vector3 spawnDir)
    {
        if (clonePrefab != null)
        {
            GameObject clone = GameObjectUtil.Instantiate(clonePrefab, transform.position+spawnDir*transform.GetComponent<CircleCollider2D>().radius/2);
            Character cloneComponent = clone.GetComponent<Character>();
            cloneComponent.lastSpawn = gameObject;
            lastSpawn = clone;
            lastSpawn.layer = 11; // Justspawned layer;
            cloneComponent.Mass = subtractAmount;
        }
        Mass -= subtractAmount;
        //Debug.Log(mass);
    }
    void Merge(Character other)
    {
        if (other.Mass == Mass || other.gameObject == lastSpawn)
        {
            return;
        }
        else if (other.Mass > Mass)
        {
            other.Mass += Mass;
            Mass -= 3;
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        Character otherChar = collision.gameObject.GetComponent<Character>();
        if (otherChar != null)
        {
           // Merge(otherChar);
        }    
    }
}

