﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragIndicatorController : MonoBehaviour{

    public DragIndicatorController instance;
    Vector3 targetScale;
    public SpriteRenderer indicatorSprite;
    public float lerpTime = 0.5f;
    private Vector3 velocity;
    // Use this for initialization
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        Restart();
        PlayerController.DeathEvent += Shutdown;
    }

    void UpdateTargetScale(int radius)
    {
        targetScale = Vector3.one * (radius * 2);
    }

    void Update()
    {
        transform.position = PlayerController.playerTransform.position;
        transform.localScale = Vector3.SmoothDamp(transform.localScale, targetScale, ref velocity, lerpTime);
    }

    public void Restart()
    {
        PlayerController.DragMagnitudeChange += UpdateTargetScale;
        PlayerController.StartDragEvent += delegate { indicatorSprite.enabled = true; };
        PlayerController.StartDragEvent += delegate { transform.localScale = Vector3.zero; };
        PlayerController.StartMoveEvent += delegate { indicatorSprite.enabled = false; };
    }
    public void Shutdown()
    {
        PlayerController.DragMagnitudeChange -= UpdateTargetScale;
        PlayerController.StartDragEvent -= delegate { indicatorSprite.enabled = true; };
        PlayerController.StartDragEvent -= delegate { transform.localScale = Vector3.zero; };
        PlayerController.StartMoveEvent -= delegate { indicatorSprite.enabled = false; };
    }
}
