﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneController : Character,IRecyle
{
    public void Restart()
    {
        PlayerController.EndMoveEvent += Grow;
        PlayerController.StartMoveEvent += MoveRandomDirection;
        MoveCompleteEvent += ToggleSpawnLayer;
        timeToTravel = 1f;
    }

    public void Shutdown()
    {
        PlayerController.EndMoveEvent -= Grow;
        PlayerController.StartMoveEvent -= MoveRandomDirection;
        MoveCompleteEvent -= ToggleSpawnLayer;
    }
    public void ToggleSpawnLayer()
    {
        if (gameObject.layer == 11)
        {
            gameObject.layer = 10;
        }
    }
    public void MoveRandomDirection()
    {
        if ((Mass%3)!=0)
        {
            StopCoroutine("Move");
            Vector3 randomDirection = UnityEngine.Random.insideUnitCircle.normalized;
            StartCoroutine(Move(randomDirection,Mass%5,null));
        }
    }
    public void Grow ()
    {
        Mass++;
    }
}
