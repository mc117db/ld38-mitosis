﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodController : MonoBehaviour,IRecyle {

	public int MassValue = 10;
	// Use this for initialization
	void OnCollisionEnter2D(Collision2D other)
	{
		other.gameObject.GetComponent<Character>().Mass += MassValue;
		GameObjectUtil.Destroy(gameObject);
	}
	public void Restart()
	{
		
	}
	public void Shutdown()
	{

	}
}
